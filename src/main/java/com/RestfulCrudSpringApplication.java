package com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestfulCrudSpringApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestfulCrudSpringApplication.class, args);
	}
}
