package com.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.model.bean.Pessoa;

public interface PessoaRepository extends JpaRepository<Pessoa, Long> {

}
